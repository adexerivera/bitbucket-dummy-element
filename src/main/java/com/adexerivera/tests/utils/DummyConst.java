package com.adexerivera.tests.utils;

/**
 * @author adexerivera on 11/01/2019
 * <p>
 * Adexe Rivera
 */
public class DummyConst {

    // Routes
    public static final String PAGE_DUMMY = "";
    public static final String PAGE_DUMMY_MASTER = "admin/dummy";
    public static final String PAGE_DUMMY_MASTER_LIST_ONE = "admin/dummy/list_one";
    
    // Page titles
    public static final String TITLE_DUMMY = "Dummy";
    public static final String TITLE_DUMMY_MASTER = "Dummy | Datos Maestros";
    public static final String TITLE_DUMMY_MASTER_LIST_ONE = "Dummy | Datos Maestros | Lista Uno";

}
