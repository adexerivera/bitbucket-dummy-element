package com.adexerivera.tests.model.repositories;

import com.adexerivera.tests.model.entities.Item;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author adexerivera on 23/07/2018
 * <p>
 * Adexe Rivera
 */
@Repository
public interface ItemRepository  extends JpaRepository<Item, Long> {

    Page<Item> findBy(Pageable page);

    Page<Item> findByNameLikeIgnoreCase(String name, Pageable page);

}
