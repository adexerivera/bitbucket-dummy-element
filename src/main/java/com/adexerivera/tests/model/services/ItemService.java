package com.adexerivera.tests.model.services;

import com.adexerivera.tests.model.entities.Item;
import com.adexerivera.tests.model.repositories.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

/**
 * @author adexerivera on 23/07/2018
 * <p>
 * Adexe Rivera
 */
@Service
public class ItemService {

    @Autowired
    private final ItemRepository itemRepository;

    @Autowired
    public ItemService(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    public List<Item> findAnyMatching(Optional<String> filter) {
        return findAnyMatching(filter, null);
    }

    public List<Item> findAnyMatching(Optional<String> filter, Pageable pageable) {
        if (filter.isPresent()) {
            String repositoryFilter = "%" + filter.get() + "%";
            return itemRepository.findByNameLikeIgnoreCase(repositoryFilter, pageable).getContent();
        } else {
            return find(pageable).getContent();
        }
    }

    public Page<Item> find(Pageable pageable) {
        return itemRepository.findBy(pageable);
    }

    public JpaRepository<Item, Long> getRepository() {
        return itemRepository;
    }

    public Item createNew() {
        return new Item();
    }

    public Item save(Item entity) {
        try {
            return getRepository().saveAndFlush(entity);
        } catch (DataIntegrityViolationException e) {
            throw new DataIntegrityViolationException("There is already a item like this. Please check the item that you try to create.");
        }

    }
}
