package com.adexerivera.tests;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * @author adexerivera on 26/11/2018
 * <p>
 * Adexe Rivera
 */
@SpringBootApplication(scanBasePackages = { "com.adexerivera.tests" })
@EntityScan(basePackages = { "com.adexerivera.tests" })
@EnableJpaRepositories(basePackages = { "com.adexerivera.tests" })
@PropertySource("classpath:/dummy.properties")
public class DummyApp extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(DummyApp.class, args);
    }

}
