package com.adexerivera.tests;

import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.dependency.HtmlImport;
import com.vaadin.flow.component.polymertemplate.Id;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.templatemodel.TemplateModel;
import com.adexerivera.tests.utils.DummyConst;
import com.adexerivera.tests.views.DummyElement;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author adexerivera on 29/11/2018
 * <p>
 * Adexe Rivera
 */
@Tag("dummy-test")
@HtmlImport("src/views/dummy-test.html")
@Route(DummyConst.PAGE_DUMMY)
@PageTitle(DummyConst.TITLE_DUMMY)
public class Main extends PolymerTemplate<TemplateModel> {

    @Id("dummy")
    private DummyElement dummyElement;

    @Autowired
    public Main() {
    }
}
