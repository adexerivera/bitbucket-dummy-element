package com.adexerivera.tests.views;

import com.adexerivera.tests.model.entities.Item;
import com.adexerivera.tests.model.services.ItemService;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.HtmlImport;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.polymertemplate.Id;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;
import com.vaadin.flow.data.provider.CallbackDataProvider;
import com.vaadin.flow.data.provider.ConfigurableFilterDataProvider;
import com.vaadin.flow.data.provider.DataProvider;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.templatemodel.TemplateModel;
import org.itccanarias.dcct.components.MenuNavButton;
import org.itccanarias.dcct.components.SubMenu;
import org.itccanarias.dcct.components.Toolbar;
import org.itccanarias.dcct.components.ToolbarMenu;
import org.itccanarias.dcct.utils.MenuUtil;
import org.springframework.beans.factory.annotation.Autowired;

@Tag("dummy-element")
@HtmlImport("src/views/dummy-element/dummy-element.html")
public class DummyElement extends PolymerTemplate<TemplateModel> {

    // This is one example o Vaadin grid use through polymer integration
    @Id("grid")
    private Grid<Item> grid;
    @Id("toolbar")
    private Toolbar toolbar;

    @Autowired
    private ItemService itemService;

    @Autowired
    public DummyElement(ItemService itemService) {
        this.itemService = itemService;

        toolbar = new Toolbar();
        ToolbarMenu toolbarMenu = toolbar.getToolbarMenu();
        toolbarMenu.add(MenuNavButton.get()
                .withCaption("Settings")
                .withIcon(VaadinIcon.COGS)
                .withNavigateTo("settings"));

        SubMenu memberListTb = toolbarMenu.add(SubMenu.get()
                .withCaption("Member")
                .withIcon(VaadinIcon.USERS));

        memberListTb.add(MenuNavButton.get()
                .withCaption("Settings")
                .withIcon(VaadinIcon.COGS)
                .withNavigateTo("settings"));

        memberListTb.add(MenuNavButton.get()
                .withCaption("Member")
                .withIcon(VaadinIcon.USERS)
                .withNavigateTo("members"));

        // Example of data provider
        prepareDataProvider();

        // Example to add columns to the grid
        grid.addColumn(Item::getId).setHeader("Código").setFlexGrow(5);
        grid.addColumn(Item::getName).setHeader("Nombre").setFlexGrow(5);

        // Example of adding a column using the ComponentRenderer
        grid.addColumn(new ComponentRenderer<>(item -> {
            // Button for update
            Icon iconBtnUpdate = new Icon(VaadinIcon.PENCIL);
            iconBtnUpdate.setSize("20px");
            Button updateBtn = new Button(iconBtnUpdate);
            updateBtn.getElement().setAttribute("title", "Editar");
            updateBtn.getElement().setAttribute("style", "border-bottom-right-radius: 0; border-top-right-radius: 0;");
            updateBtn.getElement().getThemeList().add("icon contrast small");

            // Button that removes the item
            Icon iconBtnDelete = new Icon(VaadinIcon.TRASH);
            iconBtnDelete.setSize("20px");
            Button removeBtn = new Button(iconBtnDelete);
            removeBtn.getElement().setAttribute("title", "Borrar");
            removeBtn.getElement().setAttribute("style", "border-bottom-left-radius: 0; border-top-left-radius: 0; ");
            removeBtn.getElement().getThemeList().add("icon error small");

            // Layouts for placing the buttons
            HorizontalLayout buttons = new HorizontalLayout(updateBtn, removeBtn);
            buttons.setSpacing(false);
            VerticalLayout btnGroup = new VerticalLayout(buttons);
            btnGroup.setPadding(false);
            btnGroup.setSpacing(false);
            return btnGroup;
        })).setHeader("Actions").setFlexGrow(0).setWidth("150px");
    }

    private void prepareDataProvider() {

        DataProvider<Item, String> dataProvider = new CallbackDataProvider<>(
                query -> itemService.findAnyMatching(query.getFilter()).stream(),
                query -> itemService.findAnyMatching(query.getFilter()).size());
        ConfigurableFilterDataProvider<Item, Void, String> filterDataProvider = dataProvider.withConfigurableFilter();

        grid.setDataProvider(filterDataProvider);
    }

}
